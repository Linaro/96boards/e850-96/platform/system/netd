/*
 * Copyright 2021 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * ExynosClat.h - Exynos CLAT functions
 */

#pragma once

#include <netinet/in.h>
#include <netinet/in6.h>
#include <sys/types.h>

#define HW_CLAT_ADDR_MAX_LEN	(4)

class ExynosClat {
public:
	static ExynosClat *getInstance();

	int setHalReady(uint32_t ready);
	int setClatInfo(const char *v6_interface, const char* v4_interface,
		struct in6_addr *ipv6_local_subnet, struct in_addr *ipv4_local_subnet,
		struct in6_addr *plat_subnet);

private:
	ExynosClat();
	int getClatIndex(unsigned int rmnet_index);
	int setClatIfacesNum();
	int ioctlClat(unsigned int cmd, void *param);

private:
	static inline ExynosClat *sInstance = NULL;

	bool halReady;
	int clatIndexOccupied[HW_CLAT_ADDR_MAX_LEN];
};
