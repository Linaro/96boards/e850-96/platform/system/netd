/*
 * Copyright 2021 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * ExynosClat.cpp - Exynos CLAT functions
 */

#include <errno.h>
#include <fcntl.h>
#define LOG_TAG "exynos-clat"
#include <log/log.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/prctl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "ExynosClat.h"

#define EXYNOS_CLAT_DEV_NAME		"/dev/umts_toe0"
#define EXYNOS_UPSTREAM_NAME		"rmnet"

struct clat_info {
	uint32_t clat_index;
	char ipv6_iface[IFNAMSIZ];
	char ipv4_iface[IFNAMSIZ];
	struct in6_addr ipv6_local_subnet;
	struct in_addr ipv4_local_subnet;
	struct in6_addr plat_subnet;
} __attribute__((__packed__));

#define IOCTL_TOE_MAGIC					('T')
#define IOCTL_TOE_SET_CLAT_READY		_IOW(IOCTL_TOE_MAGIC, 0x00, uint32_t)
#define IOCTL_TOE_SET_CLAT_IFACES_NUM	_IOW(IOCTL_TOE_MAGIC, 0x01, uint32_t)
#define IOCTL_TOE_SET_CLAT_INFO			_IOW(IOCTL_TOE_MAGIC, 0x02, struct clat_info)

ExynosClat::ExynosClat()
{
	halReady = false;

	for (unsigned int i = 0; i < HW_CLAT_ADDR_MAX_LEN; i++) {
		clatIndexOccupied[i] = -1;
	}
}

ExynosClat *ExynosClat::getInstance()
{
	if (sInstance)
		return sInstance;

	sInstance = new ExynosClat();
	if (!sInstance)
		return NULL;

	return sInstance;
}

int ExynosClat::getClatIndex(unsigned int rmnet_index)
{
	// check the same index if possible
	for (unsigned int i = 0; i < HW_CLAT_ADDR_MAX_LEN; i++) {
		if (clatIndexOccupied[i] == (int)rmnet_index)
			return i;
	}

	// find empty
	for (unsigned int i = 0; i < HW_CLAT_ADDR_MAX_LEN; i++) {
		if (clatIndexOccupied[i] < 0) {
			clatIndexOccupied[i] = (int)rmnet_index;
			return i;
		}
	}

	return -1;
}

int ExynosClat::ioctlClat(unsigned int cmd, void *param)
{
	int ret;
	int fd;

	fd = open(EXYNOS_CLAT_DEV_NAME, O_RDWR | O_CLOEXEC);
	if(fd < 0) {
		ALOGE("failed to open clat dev:%s (%s)", EXYNOS_CLAT_DEV_NAME, strerror(errno));
		return fd;
	}

	ret = ioctl(fd, cmd, param);
	if (ret < 0)
		ALOGE("failed to ioctl cmd:0x%X (%s)", cmd, strerror(errno));

	close(fd);
	return ret;
}

int ExynosClat::setHalReady(uint32_t ready)
{
	int ret;

	ret = ioctlClat(IOCTL_TOE_SET_CLAT_READY, &ready);
	if (ret) {
		ALOGE("failed to set clat hal ready:%d ret:%d", ready, ret);
		goto out;
	}

	ALOGE("set clat hal ready %d", ready);
	halReady = ready ? true : false;

out:
	return ret;
}

int ExynosClat::setClatIfacesNum()
{
	uint32_t num = 0;
	int ret;

	for (unsigned int i = 0; i < HW_CLAT_ADDR_MAX_LEN; i++) {
		if (clatIndexOccupied[i] >= 0)
			num++;
	}

	ret = ioctlClat(IOCTL_TOE_SET_CLAT_IFACES_NUM, &num);
	if (ret) {
		ALOGE("failed to set the number of clat ifaces ret:%d", ret);
		return num;
	}

	ALOGE("set #clat_ifaces %d", num);
	return num;
}

int ExynosClat::setClatInfo(const char *ipv6_interface, const char* ipv4_interface,
		struct in6_addr *ipv6_local_subnet, struct in_addr *ipv4_local_subnet,
		struct in6_addr *plat_subnet)
{
	struct clat_info info;
	unsigned int prefix_len;
	unsigned int rmnet_index;
	int clat_index;
	int num;
	int ret = 0;

	memset(&info, 0, sizeof(info));

	prefix_len = strlen(EXYNOS_UPSTREAM_NAME);
	if (!ipv6_interface || strncmp(ipv6_interface, EXYNOS_UPSTREAM_NAME, prefix_len) != 0) {
		ALOGE("unexpected upstream name:%s", ipv6_interface);
		ret = -EINVAL;
		goto out;
	}

	rmnet_index = strtoul(ipv6_interface + prefix_len, NULL, 0);
	clat_index = getClatIndex(rmnet_index);

	if (clat_index < 0) {
		ALOGE("clat indexes are full rmnet:%d", rmnet_index);
		ret = -EBUSY;
		goto out;
	}

	info.clat_index = (uint32_t)clat_index;
	strncpy(info.ipv6_iface, ipv6_interface, IFNAMSIZ);

	if (!ipv4_interface || !ipv6_local_subnet || !ipv4_local_subnet || !plat_subnet) {
		ALOGE("reset clat info for rmnet:%d clat:%d", rmnet_index, info.clat_index);
		clatIndexOccupied[info.clat_index] = -1;
		goto set_hw;
	}

	ALOGE("set clat info for rmnet:%d clat:%d", rmnet_index, info.clat_index);

	strncpy(info.ipv4_iface, ipv4_interface, IFNAMSIZ);
	memcpy(&info.ipv6_local_subnet, ipv6_local_subnet, sizeof(struct in6_addr));
	memcpy(&info.ipv4_local_subnet, ipv4_local_subnet, sizeof(struct in_addr));
	memcpy(&info.plat_subnet, plat_subnet, sizeof(struct in6_addr));

set_hw:
	ret = ioctlClat(IOCTL_TOE_SET_CLAT_INFO, &info);
	if (ret)
		ALOGE("failed to set clat info ret:%d", ret);

out:
	/*
	 * Reset ready state if ioctl is failed.
	 * But recover the state when all clat ifaces are cleared.
	 */
	num = setClatIfacesNum();
	if (num && (ret || !halReady))
		setHalReady(0);
	else
		setHalReady(1);

	return ret;
}
